const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { secret } = require('../configs/auth');

module.exports = (request, response, next) => {

  const authHeader = request.headers['authorization'];
  
  if(!authHeader) {
    return response.status(401).json({message: "Failed to fetch"});
  }
  const [, jwt_token] = authHeader.split(' ');

  try {
    // const { _id } = jwt.verify(jwt_token, secret);
    request.registeredUser = jwt.verify(jwt_token, secret);
    next();
  } catch (err) {
    return response.status(401).json({message: "Bad request"});
  }
};