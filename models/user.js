const { model, Schema } = require('mongoose');

const userSchema = new Schema({ 
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  } 
});

  // assign a function to the "methods" object of our animalSchema
  userSchema.methods.selectPublicFields = function() {
    const { _id, username, createdDate } = this;
    return { _id, username, createdDate };
  };

  module.exports = model('user', userSchema);