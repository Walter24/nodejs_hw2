const mongoose = require('mongoose');

module.exports = mongoose.model('note', {
  userId: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean
  },
  text: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  }
});