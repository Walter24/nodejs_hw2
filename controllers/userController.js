const User = require('../models/user');


module.exports.getUser = (req, res) => {
    try {
      const { _id, username, createdDate } = req.registeredUser;
      res.json({user: {_id, username, createdDate}});
    } catch(err) {
      console.log(err);
      res.status(500).json({message: "Internal server error"});
    };
};


module.exports.deleteUsers = (req, res) => {
  User.findByIdAndDelete(req.registeredUser._id).exec()
    .then(() => {
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({error: err.message});
    });
};


module.exports.changePassword = (req, res) => {
  
  if (req.body.oldPassword === req.registeredUser.password) {
    return User.findByIdAndUpdate(req.registeredUser._id, {$set: {password: req.body.newPassword}}).exec()
    .then(() => {
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({message: "Internal server error"});
    });
  }
  return res.status(400).json({message: "Bad request"});  
};

