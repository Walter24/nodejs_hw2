const jwt = require('jsonwebtoken');

const User = require('../models/user');

const { secret } = require('../configs/auth');

module.exports.register = (request, response) => {
  const { username, password } = request.body;
  const user = new User({username, password});
  user.save()
    .then(() => {
      response.json({message: "Success"});
    })
    .catch(err => {
      response.status(500).json({message: "Internal server error"});
    });
};

module.exports.login = (request, response) => {
  const { username, password } = request.body;
  User.findOne({username, password}).exec()
    .then(user => {
      if(!user) {
        return response.status(400).json({message: "Bad request"});
      }
      response.json({message: "Success", jwt_token: jwt.sign(JSON.stringify(user), secret)});
    })
    .catch(err => {
      response.status(500).json({message: "Internal server error"});
    });
};