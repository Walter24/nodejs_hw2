const Note = require('../models/note');

module.exports.addNote = (req, res) => {
  const userId = req.registeredUser._id;
  const { completed, text } = req.body;

  const note = new Note({userId, completed, text});
  note.save()
    .then(() => {
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    });
};

module.exports.getNotes = (req, res) => {
  Note.find({}).exec()
    .then(notes => {
      res.json({notes});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    });
};

module.exports.getNoteById = (req, res) => {
  Note.findById(req.params.id).exec()
    .then(note => {
      res.json({note});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    });
};

module.exports.updateNoteById = (req, res) => {
  Note.findByIdAndUpdate(req.params.id, {$set: req.body}).exec()
    .then(() => {
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    });
};

module.exports.checkNoteById = (req, res) => {
  Note.findByIdAndUpdate(req.params.id, {$set: {completed: req.body.completed}}).exec()
    .then(note => {
      if(!note) {
        res.status(400).json({message: "Bad request"});
      }
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    });
};

module.exports.deleteNote = (req, res) => {
  Note.findByIdAndDelete(req.params.id).exec()
    .then(note => {
      if(!note) return res.status(400).json({message: 'Note not found'});
      res.json({message: "Success"});
    })
    .catch(err => {
      res.status(500).json({message: err.message});
    })
};
