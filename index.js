const express = require('express');
const mongoose = require('mongoose');
const app = express();

const { port } = require('./configs/server');
const dbConfig = require('./configs/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

// mongoose.connect(`mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.databaseName}`, {
mongoose.connect(`mongodb+srv://Volodymyr:7675502@cluster0.z7ztq.mongodb.net/lab_fe14hw2`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

app.listen(port, () => {
  console.log(`Server runs on port: ${port}`);
});
