const express = require('express');
const router = express.Router();

const { getUser, deleteUsers, changePassword } = require('../controllers/userController');


const middleware = require('../middlewares/authMiddleware');

router.get('/users/me', middleware, getUser);
router.delete('/users/me', middleware, deleteUsers);
router.patch('/users/me', middleware, changePassword);

module.exports = router;