const express = require('express');
const router = express.Router();

const { addNote, getNotes, getNoteById, updateNoteById, checkNoteById, deleteNote } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');

router.post('/notes', authMiddleware, addNote);
router.get('/notes', authMiddleware, getNotes);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, checkNoteById);
router.delete('/notes/:id', authMiddleware, deleteNote);

module.exports = router;